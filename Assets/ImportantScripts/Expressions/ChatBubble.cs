using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChatBubble : MonoBehaviour
{
    // Start is called before the first frame update
    public Camera cam;
    public AnimationCurve animationCurve;
    public Image sp;
    public TMP_Text textMesh;
    public float defaultDelay = 2f;

    void Start()
    {


    }
    public void Update()
    {
        if (cam != null)
        {
            transform.LookAt(cam.transform);
        }
    }

    public void variablePlay(string message, int messageTime)
    {
        Color tmp = sp.color;
        Color tmpText = textMesh.color;
        tmp.a = 1;
        tmpText.a = 1;
        sp.color = tmp;
        textMesh.color = tmpText;
        textMesh.SetText(message);
        LeanTween.cancel(gameObject);



        gameObject.transform.localScale = new Vector3(0, 0, 0);
        gameObject.SetActive(true);
        LeanTween.scale(gameObject, new Vector3(1, 1, 1), .25f).setEase(animationCurve);
        LeanTween.value(gameObject, 1, 0, 1).setOnUpdate((value) =>
               {
                   Color tmp = sp.color;
                   tmp.a = value;
                   sp.color = tmp;

                   Color tmpText = textMesh.color;
                   tmpText.a = value;
                   textMesh.color = tmpText;

               }
            ).setDelay(messageTime);
    }
    public void basicPlay(string message)
    {
        Color tmp = sp.color;
        Color tmpText = textMesh.color;
        tmp.a = 1;
        tmpText.a = 1;
        sp.color = tmp;
        textMesh.color = tmpText;
        textMesh.SetText(message);
        LeanTween.cancel(gameObject);



        gameObject.transform.localScale = new Vector3(0, 0, 0);
        gameObject.SetActive(true);
        LeanTween.scale(gameObject, new Vector3(1, 1, 1), .25f).setEase(animationCurve);
        LeanTween.value(gameObject, 1, 0, 1).setOnUpdate((value) =>
               {
                   Color tmp = sp.color;
                   tmp.a = value;
                   sp.color = tmp;

                   Color tmpText = textMesh.color;
                   tmpText.a = value;
                   textMesh.color = tmpText;

               }
            ).setDelay(defaultDelay);
    }

}
