using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exclamation : MonoBehaviour
{
    // Start is called before the first frame update
    public Camera cam;
    public AnimationCurve animationCurve;
    private SpriteRenderer sp;
    void Start()
    {
        sp = GetComponent<SpriteRenderer>();
    }
    public void Update()
    {
        if (cam != null)
        {
            transform.LookAt(cam.transform);
        }
    }

    public void play()
    {
        Color tmp = sp.color;
        tmp.a = 1;
        sp.color = tmp;
        LeanTween.cancel(gameObject);
        gameObject.transform.localScale = new Vector3(0, 0, 0);
        gameObject.SetActive(true);
        LeanTween.scale(gameObject, new Vector3(1, 1, 1), .25f).setEase(animationCurve);
        LeanTween.value(gameObject, 1, 0, 1).setOnUpdate((value) =>
           {
               Color tmp = sp.color;
               tmp.a = value;
               sp.color = tmp;
           }
        ).setDelay(1);
    }

}
