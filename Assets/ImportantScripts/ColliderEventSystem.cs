using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColliderEventSystem : MonoBehaviour
{
    [SerializeField] private UnityEvent<Collider> onEnterTrigger;
    [SerializeField] private UnityEvent<Collider> onExitTrigger;
    [SerializeField] private UnityEvent<Collider> onStayTrigger;

    private void OnTriggerEnter(Collider collision)
    {
        onEnterTrigger.Invoke(collision);
    }
    private void OnTriggerExit(Collider collision)
    {
        onExitTrigger.Invoke(collision);
    }
    private void OnTriggerStay(Collider collision)
    {
        onStayTrigger.Invoke(collision);
    }
}
