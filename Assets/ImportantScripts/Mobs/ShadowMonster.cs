using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowMonster : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed;
    public GameObject target;
    public GameObject ghost;
    public Material material;
    public Exclamation exclamation;
    private Material genMat;
    private GameObject meshRenderer;
    void Start()
    {
        genMat = Instantiate<Material>(material);
        meshRenderer = transform.Find("evilGhostAnim").gameObject;
        this.GetComponentInChildren<SkinnedMeshRenderer>().material = genMat;
        Color temp = genMat.color;
        temp.a = 0;
        genMat.color = temp;
        LeanTween.value(0, 1, 1).setOnUpdate((value) =>
           {
               Color tempTween = genMat.color;
               tempTween.a = value;
               genMat.color = tempTween;
           });

    }

    // Update is called once per frame
    void Update()
    {
        float step = speed * Time.deltaTime; // calculate distance to move
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);
        this.transform.LookAt(target.transform);
    }

    public void collision(Collider col)
    {
        if (col.tag == "Ghost")
        {
            Rigidbody rb = gameObject.GetComponent<Rigidbody>();
            Vector3 direction = (this.transform.position - col.transform.position).normalized;
            rb.AddForce(direction * 10, ForceMode.Impulse);
            rb.AddTorque(new Vector3(0, 0, 100), ForceMode.Force);

            rb.useGravity = true;
            gameObject.GetComponent<SphereCollider>().enabled = false;
            speed = 0;
            exclamation.play();
            LeanTween.value(1, 0, .5f).setOnUpdate((value) =>
            {
                Color temp = genMat.color;
                temp.a = value;
                genMat.color = temp;
            }
          ).setOnComplete(destroyMe).setDelay(1);
        }
        else if (col.tag == "Player")
        {
            ghost.GetComponent<GhostManager>().Kill();
        }



    }
    public void activate(Collider col)
    {
        if (col.tag == "Player")
        {
            gameObject.SetActive(true);
        }
    }

    public void destroyMe()
    {
        if (this != null)
        {
            Destroy(gameObject);
        }
        //Destroy(gameObject);
    }

}
