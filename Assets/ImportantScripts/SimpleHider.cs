using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleHider : MonoBehaviour
{
    public bool hidden = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Hide()
    {
        if(!hidden){
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<MeshCollider>().enabled = false;
            hidden = true;
        }
    }
    public void Show() {
        if(hidden){
            GetComponent<MeshRenderer>().enabled = true;
            GetComponent<MeshCollider>().enabled = true;
            hidden = false;
        }
    }
}
