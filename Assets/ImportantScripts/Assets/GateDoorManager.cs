using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GateDoorManager : MonoBehaviour
{
    // Start is called before the first frame update
    private bool isOpen=false;
    public float openTo = 90f;
    public float timeToOpen = 1f;

    private Vector3 startingRotation;
    void Start()
    {
        startingRotation = transform.rotation.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Open()
    {
        var newRot = startingRotation;
        newRot.y = openTo;
        LeanTween.rotateLocal(transform.gameObject,newRot,timeToOpen);
    }

    public void Close()
    {
        LeanTween.rotateLocal(transform.gameObject,startingRotation,timeToOpen);
    }
}
