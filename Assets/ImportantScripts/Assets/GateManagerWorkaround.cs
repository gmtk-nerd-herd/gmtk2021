using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateManagerWorkaround : MonoBehaviour
{
    GateDoorManagerWorkaround[] gates;
    // Start is called before the first frame update
    void Start()
    {
        gates = GetComponentsInChildren<GateDoorManagerWorkaround>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Open() {
        gates[0].Open();
        gates[1].Open();
    }
    public void Close() {
        gates[0].Close();
        gates[1].Close();
    }
}
