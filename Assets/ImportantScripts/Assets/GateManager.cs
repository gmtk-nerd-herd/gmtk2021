using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Open() {
        var gates = GetComponentsInChildren<GateDoorManager>();
        for (int i = 0; i < gates.Length; i++)
        {
            gates[i].Open();
        }
    }
    public void Close() {
        var gates = GetComponentsInChildren<GateDoorManager>();
        for (int i = 0; i < gates.Length; i++)
        {
            gates[i].Close();
        }
    }
}
