using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TransitionHandler : MonoBehaviour
{
    public LeanTweenType animCurve;
    public Image restartLevelTransition;
    public Image nextLevelTransition;
    public Image backdrop;

    public Loader.Scene restartScene;
    public Loader.Scene nextScene;
    // Start is called before the first frame update

    public void Start()
    {
        Color temp = backdrop.color;
        temp.a = 1;
        backdrop.color = temp;
        LeanTween.value(1, 0, 1).setOnUpdate((value) =>
       {
           Color temp = backdrop.color;
           temp.a = value;
           backdrop.color = temp;
       }).setEase(animCurve);

    }
    public void nextLevel(Collider col)
    {
        if (col.tag == "Player")
        {
            LeanTween.value(0, 1, 1).setOnUpdate((value) =>
                            {
                                Color temp = nextLevelTransition.color;
                                temp.a = value;
                                nextLevelTransition.color = temp;
                            }).setEase(animCurve).setOnComplete(fadeNextLevel);
        }
    }
    public void nextLevelNoCollider()
    {

        LeanTween.value(0, 1, 1).setOnUpdate((value) =>
                        {
                            Color temp = nextLevelTransition.color;
                            temp.a = value;
                            nextLevelTransition.color = temp;
                        }).setEase(animCurve).setOnComplete(fadeNextLevel);

    }
    public void fadeNextLevel()
    {
        LeanTween.value(1, 0, 1).setOnUpdate((value) =>
                        {
                            Color temp = restartLevelTransition.color;
                            temp.a = value;
                            restartLevelTransition.color = temp;
                        }).setEase(animCurve).setDelay(2);
        LeanTween.value(0, 1, 1).setOnUpdate((value) =>
        {
            Color temp = backdrop.color;
            temp.a = value;
            backdrop.color = temp;
        }).setEase(animCurve).setOnComplete(loadNextScene).setDelay(2);
    }

    public void restartCollider(Collider col)
    {
        if (col.tag == "Player")
        {
            LeanTween.value(0, 1, 1).setOnUpdate((value) =>
                     {
                         Color temp = restartLevelTransition.color;
                         temp.a = value;
                         restartLevelTransition.color = temp;
                     }).setEase(animCurve).setOnComplete(fadeRestart);
        }

    }
    public void restart()
    {

        LeanTween.value(0, 1, 1).setOnUpdate((value) =>
                 {
                     Color temp = restartLevelTransition.color;
                     temp.a = value;
                     restartLevelTransition.color = temp;
                 }).setEase(animCurve).setOnComplete(fadeRestart);


    }

    public void fadeRestart()
    {
        LeanTween.value(1, 0, 1).setOnUpdate((value) =>
                        {
                            Color temp = restartLevelTransition.color;
                            temp.a = value;
                            restartLevelTransition.color = temp;
                        }).setEase(animCurve).setDelay(2);
        LeanTween.value(1, 1, 1).setOnUpdate((value) =>
        {
            Color temp = backdrop.color;
            temp.a = value;
            backdrop.color = temp;
        }).setEase(animCurve).setOnComplete(sceneRestart).setDelay(2);
    }

    public void loadNextScene()
    {
        Loader.Load(nextScene);
    }
    public void sceneRestart()
    {
        Loader.Load(restartScene);
    }
}
