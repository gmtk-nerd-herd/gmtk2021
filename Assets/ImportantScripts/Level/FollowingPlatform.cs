using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowingPlatform : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject ghost;
    private float[] rotationBuffer = new float[100];
    private bool disable = false;
    public bool restrictMovement = true;
    public bool smoothRotation = true;
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(disable) return;
        Vector3 posdif = ghost.transform.position - transform.position;
        float rotationZ = (Mathf.Atan2(posdif.y, posdif.x)-Mathf.PI/2) * Mathf.Rad2Deg;
        float finalRotation = rotationZ;
        if(smoothRotation){
            var sum = 0f;
            for (int i = rotationBuffer.Length-1; i > 0; i--)
            {
                rotationBuffer[i]=rotationBuffer[i-1];
                sum+=rotationBuffer[i];
            }
            rotationBuffer[0] = rotationZ;
            sum += rotationZ;
            finalRotation = (float)sum/rotationBuffer.Length;
        }
        
        if(restrictMovement){
            if(finalRotation>26.1f) finalRotation = 26.1f;
            else if(finalRotation< -24.8f) finalRotation = -24.8f;
        }
        
        
        transform.rotation = Quaternion.Euler(0.0f, 0.0f, finalRotation);
    }

    public void setDisable(bool _d)
    {
        disable = _d;
    }
}
