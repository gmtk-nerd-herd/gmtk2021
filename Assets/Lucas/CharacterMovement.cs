using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    // Start is called before the first frame update
    public bool moveRight = true;
    public float moveSpeed = 2.0f;
    public float waitOnBeckon = 1.5f;
    public float turnTime=0.3f;
    public float currentMoveSpeed = 0;
    private Animator animator;
    private GameObject meshRenderer;

    CharacterController characterController;
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        currentMoveSpeed = moveSpeed;
        animator=GetComponentInChildren<Animator>();
        meshRenderer=this.transform.Find("kneelAnim").gameObject;
    }
    void FixedUpdate()
    {
        Vector3 force = new Vector3(currentMoveSpeed, 0, 0);
        if (!moveRight) force *= -1;
        characterController.SimpleMove(force);
        animator.SetFloat("Speed",Mathf.Abs(characterController.velocity.x/moveSpeed));
        animator.SetBool("Grounded",characterController.velocity.y>=-2);
        LeanTween.rotateY(meshRenderer,180+(-90*characterController.velocity.x/moveSpeed),turnTime);
    }

    public void Beckon(Vector3 ghostPosition)
    {
        moveRight = ghostPosition.x > transform.position.x;
        //stop, wait for a bit, then set move speed back
        StartCoroutine(BeckonCoroutine());
    }
    IEnumerator BeckonCoroutine()
    {
        if(waitOnBeckon>0) StartCoroutine(ChangeSomeValue(currentMoveSpeed,0,waitOnBeckon/4));
        transform.GetChild(0).GetComponent<Exclamation>().play();
        yield return new WaitForSeconds(waitOnBeckon);
        if(waitOnBeckon>0) StartCoroutine(ChangeSomeValue(currentMoveSpeed,moveSpeed,waitOnBeckon/4));
        animator.SetBool("Depressed",false);
    }
    IEnumerator ChangeSomeValue(float oldValue, float newValue, float duration) {
        for (float t = 0f; t < duration; t += Time.deltaTime) {
            currentMoveSpeed = Mathf.Lerp(oldValue, newValue, t / duration);
            yield return null;
        }
        currentMoveSpeed = newValue;
    }
    public void stopMoving(Collider col)
    {
        if (col.tag == "Player")
        {
            currentMoveSpeed = 0;
            animator.SetBool("Depressed",true);
        }
    }
    public void Kill(){
        currentMoveSpeed = 0;
        animator.SetTrigger("Kill");
        //animation junk
    }
}
