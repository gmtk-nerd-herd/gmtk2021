using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GhostManager : MonoBehaviour
{
    // Start is called before the first frame update
    public Cinemachine.CinemachineVirtualCamera cinemachineVirtualCamera;
    Vector3 currentMove = Vector3.zero;
    Vector3 targetLocation = Vector3.zero;

    private GameObject meshRenderer;

    //public options
    public float smoothTime = 0.3f;

    public float accelX = 0;
    public float accelY = 0;
    public Vector3 movementSpeed = new Vector3(1, 1, 1);
    private CharacterController characterController;
    public GameObject livingCharacter;
    public float restartWaittime = 1;
    public TransitionHandler transitionHandler;
    public bool isAlive = true;
    public float ShakeSpeed = 20.0f; //how fast it shakes
    public float ShakeAmount = 0.01f; //how much it shakes
    private bool end = false;

    private Vector3 velocity = Vector3.zero;
    private float referenceXSpeed = 0;
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        targetLocation = characterController.transform.position;
        meshRenderer = transform.Find("ghostAnim").gameObject;
    }


    void Update()
    {
    }
    void FixedUpdate()
    {
        if (!isAlive) return;

        if (currentMove.Equals(Vector2.zero))
        {
            float xMult = Mathf.SmoothDamp(accelX, 0, ref velocity.x, 0.005f);
            float yMult = Mathf.SmoothDamp(accelY, 0, ref velocity.y, 0.005f);
            currentMove
             = new Vector2(characterController.velocity.x * xMult, characterController.velocity.y * yMult);

        }
        characterController.Move(currentMove * Time.deltaTime);

        // Aesthetic stuff (shake and rotate)
        if (Vector3.Distance(transform.position, livingCharacter.transform.position) >= 7)
        {
            characterController.Move(new Vector3((Mathf.Sin(Time.fixedTime * ShakeSpeed) * ShakeAmount), 0, 0));
        }
        LeanTween.rotateY(meshRenderer, 180 + (-90 * referenceXSpeed), smoothTime);

        // Old Version
        // targetLocation = currentMove * Time.deltaTime;
        // float updatedPosX = Mathf.SmoothDamp(characterController.transform.position.x, targetLocation.x, ref velocity.x, smoothTime);
        // float updatedPosY = Mathf.SmoothDamp(characterController.transform.position.y, targetLocation.y, ref velocity.y, smoothTime);
        // float updatedPosZ = Mathf.SmoothDamp(characterController.transform.position.z, targetLocation.z, ref velocity.z, smoothTime);
        //characterController.transform.position = new Vector3(updatedPosX, updatedPosY, updatedPosZ);
        //Debug.Log(new Vector3(updatedPosX, updatedPosY, updatedPosZ));
        //characterController.Move((gameObject.transform.position - targetLocation) * Time.deltaTime);
    }
    public void OnMove(InputValue input)
    {
        Vector2 inputVec = input.Get<Vector2>();
        float xMult;
        float yMult;

        xMult = Mathf.SmoothDamp(accelX, 1, ref velocity.x, .005f);
        yMult = Mathf.SmoothDamp(accelY, 1, ref velocity.y, .005f);
        inputVec = new Vector2(inputVec.x * xMult, inputVec.y * yMult);


        currentMove = inputVec * movementSpeed;
        referenceXSpeed = inputVec.x;

        // Old version
        // Vector2 inputVec = input.Get<Vector2>();
        // currentMove = inputVec * movementSpeed;


    }
    public void OnBeckon(InputValue input)
    {
        livingCharacter.GetComponent<CharacterMovement>().Beckon(transform.position);
        transform.GetChild(0).GetComponent<Exclamation>().play();
    }
    public void gameEnd(Collider col)
    {
        if (col.tag == "Player")
        {
            end = true;
            print("WHY");
            cinemachineVirtualCamera.m_LookAt = livingCharacter.transform;
        }
    }
    public void Kill()
    {
        if (!isAlive) return;
        isAlive = false;
        if (end) return;
        livingCharacter.GetComponent<CharacterMovement>().Kill();
        StartCoroutine(WaitRestart());
        //kill the ghost on tether break
        //run kill animation
    }
    IEnumerator WaitRestart()
    {
        yield return new WaitForSeconds(restartWaittime);
        transitionHandler.restart();
    }
}
