using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class introTransition : MonoBehaviour
{
    // Start is called before the first frame update
    public Image one;
    public Image two;
    public Image three;
    public LeanTweenType ease;
    public TransitionHandler next;

    void Start()
    {
        Color temp = one.color;
        temp.a = 0;
        one.color = temp;
        LeanTween.value(0, 1, 3).setOnUpdate((value) =>
          {
              Color tempTween = one.color;
              tempTween.a = value;
              one.color = tempTween;
          }).setEase(ease);
        LeanTween.scale(one.rectTransform, new Vector3(1.2f, 1.2f, 1.2f), 3);
        LeanTween.value(1, 0, 1.5f).setOnUpdate((value) =>
          {
              Color tempTween = one.color;
              tempTween.a = value;
              one.color = tempTween;
          }).setEase(ease).setDelay(3).setOnComplete(twoFunc);





    }

    public void twoFunc()
    {
        Color temp = two.color;
        temp.a = 0;
        two.color = temp;
        LeanTween.value(0, 1, 3).setOnUpdate((value) =>
          {
              Color tempTween = two.color;
              tempTween.a = value;
              two.color = tempTween;
          }).setEase(ease);
        LeanTween.scale(two.rectTransform, new Vector3(1.2f, 1.2f, 1.2f), 3);
        LeanTween.value(1, 0, 1.5f).setOnUpdate((value) =>
          {
              Color tempTween = two.color;
              tempTween.a = value;
              two.color = tempTween;
          }).setEase(ease).setDelay(3).setOnComplete(threeFunc);
    }
    public void threeFunc()
    {
        Color temp = three.color;
        temp.a = 0;
        three.color = temp;
        LeanTween.value(0, 1, 3).setOnUpdate((value) =>
          {
              Color tempTween = three.color;
              tempTween.a = value;
              three.color = tempTween;
          }).setEase(ease);
        LeanTween.scale(three.rectTransform, new Vector3(1.2f, 1.2f, 1.2f), 3);
        LeanTween.value(1, 0, 1.5f).setOnUpdate((value) =>
         {
             Color tempTween = three.color;
             tempTween.a = value;
             three.color = tempTween;
         }).setEase(ease).setDelay(3).setOnComplete(transition);
    }

    public void transition()
    {
        next.nextLevelNoCollider();
    }


}
