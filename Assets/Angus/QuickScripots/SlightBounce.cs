using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlightBounce : MonoBehaviour
{
    // Start is called before the first frame update
    public LeanTweenType ease;
    public float time = 1;
    GameObject obj;
    void Start()
    {
        obj = gameObject;
        LeanTween.move(obj, (obj.transform.position + (Vector3.up)), time).setLoopPingPong().setEase(ease);
    }


}
