using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockEnable : MonoBehaviour
{
    public void blockEnable(Collider col)
    {
        if (col.tag == "Player")
        {
            gameObject.SetActive(true);
        }
    }
}
