using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAutoWalk : MonoBehaviour
{
    // Start is called before the first frame update
    private CharacterController characterController;
    public float speed = 1;
    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        characterController.SimpleMove(Vector3.right * speed);
    }
}
