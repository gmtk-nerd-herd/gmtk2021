using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{

    public Vector3 endPosition;
    public LeanTweenType ease;
    public float time;
    // Start is called before the first frame update
    void Start()
    {
        LeanTween.move(gameObject, endPosition, time).setLoopPingPong().setEase(ease);
    }

    public void enter(Collider col)
    {
        if (col.tag == "Player")
        {
            col.transform.parent = gameObject.transform;

        }
    }
    public void exit(Collider col)
    {
        if (col.tag == "Player")
        {
            col.transform.parent = null;

        }
    }
}
