using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireController : MonoBehaviour
{
    private GameObject fire;
    private ParticleSystem fireParticles;
    public bool StartActive;
    // Start is called before the first frame update
    void Start()
    {
        fire=transform.Find("Fire_VFX").gameObject;
        fire.SetActive(StartActive);
        fireParticles=transform.GetComponentInChildren<ParticleSystem>();
    }

    public void FireOn(){
        fire.SetActive(true);
    }
    public void FireOff(){
        fire.SetActive(false);
        fireParticles.Play();
    }
}
