using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PulserController : MonoBehaviour
{
    [SerializeField] private UnityEvent turnOn;
    [SerializeField] private UnityEvent imminent;
    [SerializeField] private UnityEvent turnOff;
    public float OnTime=2f;
    public float OffTime=2f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(waiter());
    }

    IEnumerator waiter()
    {
        //Wait for OnTime seconds
        yield return new WaitForSeconds(OnTime*0.8f);
        imminent.Invoke();

        yield return new WaitForSeconds(OnTime*0.2f);
        turnOn.Invoke();

        //Wait for OffTime seconds
        yield return new WaitForSeconds(OffTime);

        turnOff.Invoke();

        StartCoroutine(waiter());
    }
}
