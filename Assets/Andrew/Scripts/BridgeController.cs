using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeController : MonoBehaviour
{
    public float smoothTime = 0.3f;
    public bool StartHigh;
    public float Height=1f;
    private Vector3 velocity = Vector3.zero;
    // Start is called before the first frame update
    private Vector3 targetPosition;
    private Vector3 startPosition;
    void Start()
    {
        startPosition=transform.position;
        if(StartHigh){
            setHigh();
        } else {
            setLow();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float updatedPosX = Mathf.SmoothDamp(transform.position.x,targetPosition.x,ref velocity.x,smoothTime);
        float updatedPosY = Mathf.SmoothDamp(transform.position.y,targetPosition.y,ref velocity.y,smoothTime);
        float updatedPosZ = Mathf.SmoothDamp(transform.position.z,targetPosition.z,ref velocity.z,smoothTime);
        transform.position = new Vector3(updatedPosX,updatedPosY,updatedPosZ);
    }
    public void setHigh(){
        targetPosition=startPosition+Vector3.up*Height;
        Debug.Log("BridgeUP");
    }
    public void setLow(){
        targetPosition=startPosition;
        Debug.Log("BridgeDOWN");
    }
}
