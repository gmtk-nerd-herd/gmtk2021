using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetherController : MonoBehaviour
{
    public GameObject Character;
    public float DeathLength;
    private LineRenderer tetherRenderer;
    private float distance;
    private Color lineColor;
    private AudioSource audioSource;

    private GhostManager ghost;
        // Start is called before the first frame update
    void Start()
    {
        tetherRenderer = transform.gameObject.GetComponent<LineRenderer>();
        lineColor=new Color(1,1,1,1);
        ghost = GetComponent<GhostManager>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!ghost.isAlive) return;
        //how far away are you, and should you be dead?
        distance=Vector3.Distance(transform.position,Character.transform.position);
        float deathPercent = distance/DeathLength;
        if(deathPercent>=1){
            death();
        }

        //tetherRenderer.endWidth = 0;
        AnimationCurve widthCurve = new AnimationCurve();
        var centralWidth = 1-deathPercent;
        if(centralWidth<0.1) centralWidth = 0.1f;
        widthCurve.AddKey(0,1-deathPercent/4);
        widthCurve.AddKey(0.4f, centralWidth);
        widthCurve.AddKey(0.5f, centralWidth);
        widthCurve.AddKey(0.6f, centralWidth);
        widthCurve.AddKey(1,0.1f);

        widthCurve.SmoothTangents(1,0);
        widthCurve.SmoothTangents(2,0);
        tetherRenderer.widthCurve = widthCurve;

        tetherRenderer.widthMultiplier = .8f;

        //move line
        var points = new Vector3[12];

        for (int i = 0; i < 12; i++)
        {
            points[i] = Vector3.Lerp(transform.position,Character.transform.position+Vector3.up,(float)i/11);
        }
        tetherRenderer.SetPositions(points);
    }
    public void death(){
        if(ghost.isAlive){
            ghost.Kill();
            audioSource.Play();
            Destroy(tetherRenderer);
        }
    }
}
